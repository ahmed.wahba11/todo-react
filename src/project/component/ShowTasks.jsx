import React from 'react';



const ShowTasks = props => {

    if(props.tasks.length > 0){
        return(
            <div className="row">
                <div className="col-6">
                    <h3>Pending Tasks</h3>
                    <table className="table">
                        <thead className="thead-light">
                        <th scope="col">ID</th>
                        <th scope="col">Task Name</th>
                        <th scope="col">Creator</th>
                        <th scope="col">Status</th>
                        </thead>
                        <tbody>

                        {props.tasks.map(task =>{
                            if(task.status === 'Pending'){
                                return(
                                    <tr key={task.id}>
                                        <th scope="row">{task.id}</th>
                                        <td>{task.taskName}</td>
                                        <td>{task.creator}</td>
                                        <td >{task.status} ||
                                            <button id={task.id} className="btn btn-primary"
                                                    onClick={props.changeStatusHandler}>DONE</button>
                                        </td>
                                    </tr>
                                )
                            }
                        })}

                        {/*{this.state.tasks.status.filter(tasks =>*/}
                        {/*    tasks.includes('Pending')).map(task =>*/}
                        {/*    <tr key={task.id}>*/}
                        {/*      <th scope="row">{task.id}</th>*/}
                        {/*      <td>{task.taskName}</td>*/}
                        {/*      <td>{task.creator}</td>*/}
                        {/*      <td>{task.status}</td>*/}
                        {/*    </tr>*/}
                        {/*)}*/}
                        </tbody>
                    </table>
                </div>

                <div className="col-6">
                    <h3>Completed Tasks</h3>
                    <table className="table">
                        <thead className="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Task Name</th>
                            <th scope="col">Creator</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.tasks.map(task =>{
                            if(task.status === 'Done'){
                                return(
                                    <tr key={task.id}>
                                        <th scope="row">{task.id}</th>
                                        <td>{task.taskName}</td>
                                        <td>{task.creator}</td>
                                        <td>{task.status}</td>
                                    </tr>
                                )
                            }
                        })}


                        </tbody>
                    </table>
                </div>

            </div>
        )
    }else {
        return (
            <div>
                <h1> There Is No Tasks </h1>
            </div>
        )
    }
}




export default ShowTasks;