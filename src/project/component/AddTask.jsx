import React from 'react';
import ReactDom from 'react-dom';


const AddTask = props => (
    <div className="inputs">
        <form className='from' onSubmit={props.addNewTask}>
            <div className="from-group">
                <label htmlFor="taskName" >Task Name</label>
                <input type="text" id="taskName" className="form-control"
                       onChange={props.getNewTask} value={props.newTask.taskName}/>
            </div>
            <div className="from-group">
                <label htmlFor="creator" >Creator Name</label>
                <input type="text" id="creator" className="form-control"
                       onChange={props.getNewTask} value={props.newTask.creator}/>
            </div>

            {/*<div className="from-group">*/}
            {/*  <label htmlFor="creator" >Creator Name</label>*/}
            {/*  <input type="text" id="status" className="form-control"*/}
            {/*  onChange={this.getNewTask} value={this.state.newTask.status}/>*/}
            {/*</div>*/}
            {/*<div className="from-group">*/}
            {/*  <label htmlFor="status" >Status</label>*/}
            {/*  <select id="status" className="form-control">*/}
            {/*    <option>--------</option>*/}
            {/*    <option>ToDO</option>*/}
            {/*    <option>Done</option>*/}
            {/*  </select>*/}
            {/*</div>*/}
            <div className="text-center mt-2">
                <button className="btn btn-success">Save</button>
            </div>

        </form>

    </div>
)


export default AddTask;