import React,{Component} from 'react';
import './css/main.css'
//import Tasks from "./component";
import ShowTasks from "./component/ShowTasks";
import AddTask from "./component/AddTask";




class App extends Component{
  state = {
    title:'Tasks App',
    tasks:[
      {
        id: '1',
        taskName: 'Task One',
        creator: 'Ahmed',
        status:'Pending'
      },
      {
        id: '2',
        taskName: 'Task Two',
        creator: 'Mohamed',
        status: 'Done',

      },
      {
        id: '3',
        taskName: 'Task Three',
        creator: 'Mahmoud',
        status: 'Pending',


      },
    ],
    newTask:{
      id: '',
      taskName: '',
      creator: '',
      status: '',
    }
  };

  getNewTask = e =>{
    const newTask = this.state.newTask;
    newTask[e.target.id] = e.target.value;
    this.setState({newTask})
  }

  addNewTask = e => {
      e.preventDefault();
      const newTask = this.state.newTask;
      newTask.id = parseInt(this.state.tasks[this.state.tasks.length -1].id) + 1;
      newTask.status = 'Pending';
      const tasks = [...this.state.tasks,newTask];
      const emptynewTask =
          {
            id: '',
            taskName: '',
            creator: '',
            status: '',
          }
          this.setState({tasks,newTask:emptynewTask});

    }


  changeStatusHandler = e =>{
    const tasks = this.state.tasks;

    tasks.map(task =>{
      if(task.id == e.target.id){
        const newTask = {
          ...task,
          status: 'Done',
        }
        this.setState({task:newTask})
        console.log(task.status)
      }
    })
}

render() {
//console.log(this.state.tasks[this.state.tasks.length - 1].id)
  return(
      <div id="main" className="container">
        <h1 className="header">{this.state.title}</h1>
        <AddTask newTask={this.state.newTask} getNewTask={this.getNewTask}
                 addNewTask={this.addNewTask}/>
          <hr/>
          <ShowTasks tasks={this.state.tasks}/>
        </div>
    )
  }

}

export default App;